create table client (
    id serial primary key,
    nom character varying(50),
    prenom character varying(50),
    phone_number character varying(40)
);