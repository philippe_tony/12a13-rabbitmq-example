package fr.softeam.rabbitmq.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.softeam.rabbitmq.example.model.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client,Integer> {
    List<Client> findAll();
}
