package fr.softeam.rabbitmq.example.model;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Client implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;
    private String nom;
    private String prenom;
    private String phoneNumber;
}
